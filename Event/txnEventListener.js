const { EventListener } = require('./event')

let ManufacturerEvent = new EventListener();
ManufacturerEvent.txnEventListener("manufacturer", "Admin", "autochannel",
    "ShoesShoppingChaincode", "ShoeContract", "createShoe",
    "shoe073", "Sedan", "Honda City", "White", "11/05/2021", "Honda");