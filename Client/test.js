const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication();

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "ShoesShoppingChaincode",
    "ShoeContract",
    "invokeTxn",
    "",
    "createShoe",
    "shoe-05",
    "10",
    "Crimson",
    
    "21/07/2021",
    "Bluberry",
    "1099"
).then(message => {
    console.log(message.toString());
})

