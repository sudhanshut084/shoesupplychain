const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication()

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "ShoesShoppingChaincode",
    "ShoeContract",
    "queryTxn",
    "",
    "readShoe",
    "shoe-05"
).then(message => {
    console.log(message.toString())
})