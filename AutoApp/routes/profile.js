let profile = {
    dealer: {
        "Wallet":"../Network/vars/profiles/vscode/wallets/dealer.auto.com",
        "CP": "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"
    },
    retailer : {
        "Wallet":"../Network/vars/profiles/vscode/wallets/retailer.auto.com",
        "CP": "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"
    },
    manufacturer:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/manufacturer.auto.com",
        "CP": "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"
    }


} 

module.exports = {
    profile
}
