var express = require('express');
var router = express.Router();
const {clientApplication} = require('./client');
const {Events} = require('./events')
let eventClient = new Events()
eventClient.contractEventListner("manufacturer", "Admin", "autochannel",
"ShoesShoppingChaincode", "ShoeContract", "addShoeEvent")



/* GET home page. */
router.get('/', function(req, res, next) {
  let retailerClient = new clientApplication();
 
  retailerClient.generatedAndEvaluateTxn(
      "retailer",
      "Admin",
      "autochannel", 
      "ShoesShoppingChaincode",
      "ShoeContract",
      "queryAllShoes"
  )
  .then(shoes => {
    const dataBuffer = shoes.toString();
    console.log("shoes are ", shoes.toString())
    const value = JSON.parse(dataBuffer)
    console.log("History DataBuffer is",value)
    res.render('index', { title: 'Shoe Shop', itemList: value});
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
  })
});
 
router.get('/manufacturer', function(req, res, next) {
  let manufacturerClient = new clientApplication();
  manufacturerClient.generatedAndEvaluateTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "ShoesShoppingChaincode",
    "ShoeContract",
    "queryAllShoes"
  ).then(shoes =>{
    const data =shoes.toString();
    const value = JSON.parse(data)
    res.render('manufacturer', { title: 'Manufacturer Dashboard', itemList: value });
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
  })

});
router.get('/dealer', function(req, res, next) {
  res.render('dealer', { title: 'Dealer Dashboard' });
});

router.get('/event', function(req, res, next) {
  console.log("Event Response %%%$$^^$%%$",eventClient.getEvents().toString())
  var event = eventClient.getEvents().toString()
  res.send({carEvent: event})
  // .then(array => {
  //   console.log("Value is #####", array)
  //   res.send(array);
  // }).catch(err => {
  //   console.log("errors are ", err)
  //   res.send(err)
  // })
  // res.render('index', { title: 'Dealer Dashboard' });
});


router.get('/retailer', function(req, res, next) {
  let retailerClient = new clientApplication();
  retailerClient.generatedAndEvaluateTxn(
    "retailer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "ShoeContract",
    "queryAllShoes"
  )
  .then(shoes => {
    const dataBuffer = shoes.toString();
    console.log("shoes are ", shoes.toString())
    const value = JSON.parse(dataBuffer)
    console.log("History DataBuffer is",value)
    res.render('retailer', { title: 'retailer Dashboard', itemList: value});
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
})});



router.get('/addShoeEvent', async function(req, res, next) {
  let retailerClient = new clientApplication();
  const result = await retailerClient.contractEventListner("manufacturer", "Admin", "autochannel", 
  "ShoesShoppingChaincode", "addShoeEvent")
  console.log("The result is ####$$$$",result)
  res.render('manufacturer', {view: "carEvents", results: result })
})

router.post('/manuwrite',function(req,res){

  const vin = req.body.VinNumb;
  const number = req.body.shoeNumber;
  const color = req.body.shoeColor;
  const dateofmanufacture = req.body.shoeDom;
  const brand = req.body.shoeBrand;
  const price = req.body.shoePrice;

  console.log(req)
  // console.log("Request Object",req)
  let ManufacturerClient = new clientApplication();
  
  ManufacturerClient.generatedAndSubmitTxn(
      "manufacturer",
      "Admin",
      "autochannel", 
      "ShoesShoppingChaincode",
      "ShoeContract",
      "createShoe",
      vin,number,color,dateofmanufacture,brand,price
      
    ).then(message => {
        console.log("Message is $$$$",message)
        res.status(200).send({message: "Added Shoe"})
      }
    )
    .catch(error =>{
      console.log("Some error Occured $$$$###", error)
      res.status(500).send({error:`Failed to Add`,message:`${error}`})
    });
});

router.post('/manuread',async function(req,res){
  const Qvin = req.body.QVinNumb;
  let ManufacturerClient = new clientApplication();
  
  ManufacturerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "ShoeContract",
    "readShoe", Qvin)
    .then(message => {
      
      res.status(200).send({ Cardata : message.toString() });
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to Add`,message:`${error}`})
    });

 })

 //  Get History of a car
 router.get('/itemhistory',async function(req,res){
  const shoeId = req.query.shoeId;
 
  let retailerClient = new clientApplication();
  
  retailerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "ShoeContract",
    "getShoesHistory", shoeId).then(message => {
    const dataBuffer = message.toString();
    
    const value = JSON.parse(dataBuffer)
    res.render('history', { itemList: value , title: "Shoe History"})

  });

 })

 //Register a car

 router.post('/registerCar',async function(req,res){
  const Qvin = req.body.QVinNumb;
  const CarOwner = req.body.carOwner;
  const RegistrationNumber = req.body.regNumber
  let retailerClient = new clientApplication();
  
  retailerClient.generatedAndSubmitTxn( 
    "retailer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "ShoeContract",
    "registerCar", Qvin,CarOwner,RegistrationNumber)
    .then(message => {
    
      res.status(200).send("Successfully created")
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to create`,message:`${error}`})
    });

 })
// Create order
router.post('/createOrder',async function(req,res){
  const orderNumber = req.body.orderNumber;
  const shoeNumber = req.body.shoeNumber;
  const shoeColor = req.body.shoeColor;
  const shoeBrand = req.body.shoeBrand;
  const shoePrice = req.body.shoePrice
  let DealerClient = new clientApplication();

  const transientData = {
    number: Buffer.from(shoeNumber),
    color: Buffer.from(shoeColor),
    brand: Buffer.from(shoeBrand),
    price: Buffer.from(shoePrice)
  }
  
  DealerClient.generatedAndSubmitPDC( 
    "dealer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "OrderContract",
    "createOrder", orderNumber,transientData)
    .then(message => {
      
      res.status(200).send("Successfully created")
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to create`,message:`${error}`})
    });

 })

 router.post('/readOrder',async function(req,res){
  const orderNumber = req.body.orderNumber;
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn( 
    "dealer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "OrderContract",
    "readOrder", orderNumber).then(message => {
   
    res.send({orderData : message.toString()});
  }).catch(error => {
    alert('Error occured')
  })

 })

 //Get all orders
 router.get('/allOrders',async function(req,res){
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn( 
    "dealer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "OrderContract",
    "queryAllOrders","").then(message => {
    const dataBuffer = message.toString();
    const value = JSON.parse(dataBuffer);
    res.render('orders', { itemList: value , title: "All Orders"})
    }).catch(error => {
    //alert('Error occured')
    console.log(error)
  })

 })
 //Find matching orders
 router.get('/matchOrder',async function(req,res){
  const shoeId = req.query.shoeId;
 
  let retailerClient = new clientApplication();
  
  retailerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "ShoeContract",
    "checkMatchingOrders", shoeId).then(message => {
    console.log("Message response",message)
    var dataBuffer = message.toString();
    var data =[];
    data.push(dataBuffer,shoeId)
    console.log("checkMatchingOrders",data)
    const value = JSON.parse(dataBuffer)
    let array = [];
    if(value.length) {
        for (i = 0; i < value.length; i++) {
            array.push({
               "orderId": `${value[i].Key}`,"shoeId":`${shoeId}`,
                "number": `${value[i].Record.number}`, "color":`${value[i].Record.color}`, 
                "brand":`${value[i].Record.brand}`, 
                "price": `${value[i].Record.price}`,"assetType": `${value[i].Record.assetType}`
            })
        }
    }
    console.log("Array value is ", array)
    console.log("Shoe id sent",shoeId)
    res.render('matchOrder', { itemList: array , title: "Matching Orders"})

  });

 })

 router.post('/match',async function(req,res){
  const orderId = req.body.orderId;
  const shoeId = req.body.shoeId
  let DealerClient = new clientApplication();
  DealerClient.generatedAndSubmitTxn( 
    "dealer",
    "Admin",
    "autochannel", 
    "ShoesShoppingChaincode",
    "ShoeContract",
    "matchOrder", shoeId,orderId).then(message => {
   
      res.status(200).send("Successfully Matched order")
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to Match Order`,message:`${error}`})
    });

 })



module.exports = router;


