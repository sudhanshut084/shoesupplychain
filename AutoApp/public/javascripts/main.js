
function toManuDash() {
    window.location.href='/manufacturer';
}

function swalBasic(data) {
    swal.fire({
        // toast: true,
        icon: `${data.icon}`,
        title: `${data.title}`,
        animation: true,
        position: 'center',
        showConfirmButton: true,
        footer: `${data.footer}`,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
        }
    })
}

// function swalDisplay(data) {
//     swal.fire({
//         // toast: true,
//         icon: `${data.icon}`,
//         title: `${data.title}`,
//         animation: false,
//         position: 'center',
//         html: `<h3>${JSON.stringify(data.response)}</h3>`,
//         showConfirmButton: true,
//         timer: 3000,
//         timerProgressBar: true,
//         didOpen: (toast) => {
//             toast.addEventListener('mouseenter', swal.stopTimer)
//             toast.addEventListener('mouseleave', swal.resumeTimer)
//         }
//     }) 
// }

function reloadWindow() {
    window.location.reload();
}

function ManWriteData(){
    event.preventDefault();
    const vin = document.getElementById('vinNumber').value;
    const number = document.getElementById('shoeNumber').value;
    const brand = document.getElementById('shoeBrand').value;
    const color = document.getElementById('shoeColor').value;
    const price = document.getElementById('shoePrice').value;
    const dom = document.getElementById('dom').value;
    console.log(vin+number+brand+color+price+dom);

    if (vin.length==0||number.length==0||brand.length==0||color.length==0||price.length==0||dom.length==0) {
        const data = {
            title: "You might have missed something",
            footer: "Enter all mandatory fields to add a new car",
            icon: "warning"
        }
        swalBasic(data);
        }
    else{
        fetch('/manuwrite',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({VinNumb: vin, shoeNumber: number, shoeBrand: brand, shoeColor: color, shoePrice: price, shoeDom: dom})
        })
        .then(function(response){
            if(response.status == 200) {
                const data = {
                    title: "Success",
                    footer: "Added a new Shoe",
                    icon: "success"
                }
                swalBasic(data);
            } else {
                const data = {
                    title: `Shoe with Vin Number ${vin} already exists`,
                    footer: "Vin Number must be unique",
                    icon: "error"
                }
                swalBasic(data);
            }

        })
        .catch(function(error){
            const data = {
                title: "Error in processing Request",
                footer: "Something went wrong !",
                icon: "error"
            }
            swalBasic(data);
        })    
    }
}
function ManQueryData(){

    event.preventDefault();
    const Qvin = document.getElementById('QueryVinNumbMan').value;
    
    console.log(Qvin);

    if (Qvin.length==0) {
        const data = {
            title: "Enter a Valid Qvin Number",
            footer: "This is a mandatory field",
            icon: "warning"
        }
        swalBasic(data)  
    }
    else{
        fetch('/manuread',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({QVinNumb: Qvin})
        })
        .then(function(response){
            console.log(response);
            return response.json();
        })
        .then(function (Cardata){
            dataBuf = Cardata["Cardata"]
            swal.fire({
                // toast: true,
                icon: `success`,
                title: `Current status of Shoe with Qvin ${Qvin} :`,
                animation: false,
                position: 'center',
                html: `<h3>${dataBuf}</h3>`,
                showConfirmButton: true,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            }) 
        })
        .catch(function(error){
            const data = {
                title: "Error in processing Request",
                footer: "Something went wrong !",
                icon: "error"
            }
            swalBasic(data);        
        })    
    }
}

//Method to get the history of an item
function getItemHistory(shoeId) {
    console.log("postalId", shoeId)
    window.location.href = '/itemhistory?shoeId=' + shoeId;
}

function getMatchingOrders(shoeId) {
    console.log("shoeId",shoeId)
    window.location.href = 'matchOrder?shoeId=' + shoeId;
}

function RegisterShoe(){
    console.log("Entered the register function")
    event.preventDefault();
    const QVinNumb = document.getElementById('QVinNumb').value;
    const carOwner = document.getElementById('carOwner').value;
    const regNumber = document.getElementById('regNumber').value;
    console.log(QVinNumb+carOwner+regNumber);

    if (QVinNumb.length==0||carOwner.length==0||regNumber.length==0) {
        const data = {
            title: "You have missed something",
            footer: "All fields are mandatory",
            icon: "warning"
        }
        swalBasic(data)   
    }
    else{
        fetch('/registerCar',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({QVinNumb: QVinNumb, carOwner: carOwner, regNumber: regNumber})
        })
        .then(function(response){
            if(response.status === 200){
            const data = {
                title: `Registered car ${QVinNumb} to ${carOwner}`,
                footer: "Registered car",
                icon: "success"
            }
            swalBasic(data)
            } else {
                const data = {
                    title: `Failed to register car`,
                    footer: "Please try again !!",
                    icon: "error"
                }
                swalBasic(data)           
            }
        })
        .catch(function(err){
            const data = {
                title: "Error in processing Request",
                footer: "Something went wrong !",
                icon: "error"
            }
            swalBasic(data);         
        })    
    }
}

function createOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderNumber = document.getElementById('orderNumber').value;
    const shoeNumber = document.getElementById('shoeNumber').value;
    const shoeBrand = document.getElementById('shoeBrand').value;
    const shoeColor = document.getElementById('shoeColor').value;
    const shoePrice = document.getElementById('shoePrice').value;
    console.log(orderNumber + shoeColor + shoePrice);

    if (orderNumber.length == 0 || shoeNumber.length == 0 || shoeBrand.length == 0 
        || shoeColor.length == 0|| shoePrice.length == 0) {
            const data = {
                title: "You have missed something",
                footer: "All fields are mandatory",
                icon: "warning"
            }
            swalBasic(data)  
    }
    else {
        fetch('/createOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderNumber: orderNumber, shoeNumber: shoeNumber, shoeBrand: shoeBrand, shoeColor: shoeColor,shoePrice: shoePrice })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order is created`,
                        footer: "Raised Order",
                        icon: "success"
                    }
                    swalBasic(data)

                } else {
                    const data = {
                        title: `Failed to create order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)                  }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);               
            })
    }
}

function readOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderNumber = document.getElementById('ordNum').value;
    
    console.log(orderNumber );

    if (orderNumber.length == 0) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)     
    }
    else {
        fetch('/readOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderNumber: orderNumber})
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (orderData){
                dataBuf = orderData["orderData"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Order : `,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })           
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);              
            })
    }
}

function matchOrder(orderId,shoeId) {
    if (!orderId|| !shoeId) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)   
    } else {
        fetch('/match', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderId,shoeId})
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order matched successfully`,
                        footer: "Order matched",
                        icon: "success"
                    }
                    swalBasic(data)
                } else {
                    const data = {
                        title: `Failed to match order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)                 }
            })
            
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);  
            })
    }
}


function allOrders() {
    window.location.href='/allOrders';
}


function getEvent() {
    fetch('/event', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(function (response) {
            console.log("Response is ###",response)
            return response.json()
        })
        .then(function (event) {
            dataBuf = event["shoeEvent"]
            swal.fire({
                toast: true,
                // icon: `${data.icon}`,
                title: `Event : `,
                animation: false,
                position: 'top-right',
                html: `<h5>${dataBuf}</h5>`,
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            }) 
        })
        .catch(function (err) {
            swal.fire({
                toast: true,
                icon: `error`,
                title: `Error`,
                animation: false,
                position: 'top-right',
                showConfirmButton: true,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })        
        })
}