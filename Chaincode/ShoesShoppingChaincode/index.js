/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const ShoeContract = require('./lib/shoe-contract');
const OrderContract=require('./lib/order-contract');

module.exports.ShoeContract = ShoeContract;
module.exports.OrderContract=OrderContract
module.exports.contracts = [ ShoeContract,OrderContract ];
