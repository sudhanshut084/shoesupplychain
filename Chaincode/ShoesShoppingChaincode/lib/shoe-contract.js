/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class ShoeContract extends Contract {

    async shoeExists(ctx, shoeId) {
        const buffer = await ctx.stub.getState(shoeId);
        return (!!buffer && buffer.length > 0);
    }

    async createShoe(ctx, shoeId, number,color,dateofmanufacture,brand,price) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-auto-com') {
        const exists = await this.shoeExists(ctx, shoeId);
        if (exists) {
            throw new Error(`The shoe ${shoeId} already exists`);
        }
        const asset = { 
            number,
            color,
            dateofmanufacture,
            brand,
            price,
            assetType: 'shoe',
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(shoeId, buffer);

        let addShoeEventData = { Type: 'Shoe creation', Model: brand };
        await ctx.stub.setEvent('addShoeEvent', Buffer.from(JSON.stringify(addShoeEventData)));

    }
    else{
        return `User under following MSP:${mspID} cannot able to perform this action`;
    }
}

    async readShoe(ctx, shoeId) {
        const exists = await this.shoeExists(ctx, shoeId);
        if (!exists) {
            throw new Error(`The shoe ${shoeId} does not exist`);
        }
        const buffer = await ctx.stub.getState(shoeId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateShoe(ctx, shoeId, newValue) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-auto-com') {
        const exists = await this.shoeExists(ctx, shoeId);
        if (!exists) {
            throw new Error(`The shoe ${shoeId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(shoeId, buffer);
    } else{
        return `User under following MSP:${mspID} cannot able to perform this action`;
    }
}

    async deleteShoe(ctx, shoeId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-auto-com') {
        const exists = await this.shoeExists(ctx, shoeId);
        if (!exists) {
            throw new Error(`The shoe ${shoeId} does not exist`);
        }
        await ctx.stub.deleteState(shoeId);
    }
    else{
        return `User under following MSP:${mspID} cannot able to perform this action`;
    }
}
    async queryAllShoes(ctx) {
        const queryString = {
            selector: {
                assetType: 'shoe',

            },
            sort: [{ price: "asc" }],
        };
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result);
    }

    async getShoesHistory(ctx, carId) {
        let resultIterator = await ctx.stub.getHistoryForKey(carId);
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result);
    }
    async getAllResults(iterator, isHistory) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId;
                    jsonRes.timestamp = res.value.timestamp;
                    jsonRes.Value = JSON.parse(res.value.value.toString());

                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)
            }
        }
        await iterator.close()
        return allResult;

    }




}

module.exports = ShoeContract;
