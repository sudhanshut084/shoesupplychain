/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { ShoeContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('ShoeContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new ShoeContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"shoe 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"shoe 1002 value"}'));
    });

    describe('#shoeExists', () => {

        it('should return true for a shoe', async () => {
            await contract.shoeExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a shoe that does not exist', async () => {
            await contract.shoeExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createShoe', () => {

        it('should create a shoe', async () => {
            await contract.createShoe(ctx, '1003', 'shoe 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"shoe 1003 value"}'));
        });

        it('should throw an error for a shoe that already exists', async () => {
            await contract.createShoe(ctx, '1001', 'myvalue').should.be.rejectedWith(/The shoe 1001 already exists/);
        });

    });

    describe('#readShoe', () => {

        it('should return a shoe', async () => {
            await contract.readShoe(ctx, '1001').should.eventually.deep.equal({ value: 'shoe 1001 value' });
        });

        it('should throw an error for a shoe that does not exist', async () => {
            await contract.readShoe(ctx, '1003').should.be.rejectedWith(/The shoe 1003 does not exist/);
        });

    });

    describe('#updateShoe', () => {

        it('should update a shoe', async () => {
            await contract.updateShoe(ctx, '1001', 'shoe 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"shoe 1001 new value"}'));
        });

        it('should throw an error for a shoe that does not exist', async () => {
            await contract.updateShoe(ctx, '1003', 'shoe 1003 new value').should.be.rejectedWith(/The shoe 1003 does not exist/);
        });

    });

    describe('#deleteShoe', () => {

        it('should delete a shoe', async () => {
            await contract.deleteShoe(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a shoe that does not exist', async () => {
            await contract.deleteShoe(ctx, '1003').should.be.rejectedWith(/The shoe 1003 does not exist/);
        });

    });

});
